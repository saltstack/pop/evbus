import dict_tools.data as data


async def test_get(hub):
    assert "routing_key" not in hub.ingress.internal.QUEUE
    await hub.ingress.internal.get("routing_key")
    assert "routing_key" in hub.ingress.internal.QUEUE


async def test_publish(hub):
    assert "key" not in hub.ingress.internal.QUEUE
    ctx = data.NamespaceDict(acct={"routing_key": "key"})
    await hub.ingress.internal.publish(ctx, "body")
    assert "key" in hub.ingress.internal.QUEUE
    body = await hub.ingress.internal.QUEUE["key"].get()
    assert body == "body"
