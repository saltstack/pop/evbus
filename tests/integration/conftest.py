from unittest import mock

import pop.hub
import pytest


@pytest.fixture(scope="function")
def hub(event_loop):
    hub = pop.hub.Hub()
    hub.pop.loop.CURRENT_LOOP = event_loop
    hub.pop.sub.add(dyne_name="evbus")

    with mock.patch("sys.argv", ["evbus"]):
        hub.pop.config.load(["evbus", "acct", "rend"], cli="evbus")

    yield hub
