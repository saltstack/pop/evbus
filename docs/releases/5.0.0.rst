=====
5.0.0
=====

pop-evbus 5.0.0 allows for duplicate profile names under each provider.
It also allows profile name globs for event propagation.


Duplicate profiles
==================

The previous syntax is still acceptable:

.. code-block:: sls

    kafka:
      event_profile_name:
        connection:
          bootstrap_servers: localhost:9092

When profiles are listed, the same profile name can be used to create multiple profiles for the same provider:

.. code-block:: sls

    kafka:
      - event_profile_name:
          connection:
            bootstrap_servers: localhost:9092
      - event_profile_name:
          connection:
            bootstrap_servers: my_server:9092


Profile Name matching
=====================

In the acct_file, a pattern matching plugin can be specified to be used with this acct_file.
The match plugin is applied to the entire acct_file.
The default match plugin is ``glob``.
Profile names that match event profiles with the specified plugin will be treated as if they have that profile name.
This way, if an event is configured to work with a profile of a specific name,
a single profile can match multiple events.

glob
----

Match profile names to a bash-like file glob.
I.E.

.. code-block::

    *       matches everything
    ?       matches any single character
    [seq]   matches any character in seq
    [!seq]  matches any char not in seq

.. code-block:: sls

    match_plugin: glob
    kafka:
      event_profile_*:
        connection:
          bootstrap_servers: localhost:9092

The above profile will match and be used with the following event:

.. code-block:: python

    async def my_func(hub):
        await hub.evbus.broker.put(
            routing_key="foo", body="bar", profile="event_profile_baz"
        )

regex
-----

Match the profile name to a full regular expression.
I.E.

.. code-block::

    "."      Matches any character except a newline.
    "^"      Matches the start of the string.
    "$"      Matches the end of the string or just before the newline at
             the end of the string.
    "*"      Matches 0 or more (greedy) repetitions of the preceding RE.
             Greedy means that it will match as many repetitions as possible.
    "+"      Matches 1 or more (greedy) repetitions of the preceding RE.
    "?"      Matches 0 or 1 (greedy) of the preceding RE.
    *?,+?,?? Non-greedy versions of the previous three special characters.
    {m,n}    Matches from m to n repetitions of the preceding RE.
    {m,n}?   Non-greedy version of the above.
    "\\"     Either escapes special characters or signals a special sequence.
    []       Indicates a set of characters.
             A "^" as the first character indicates a complementing set.
    "|"      A|B, creates an RE that will match either A or B.
    (...)    Matches the RE inside the parentheses.
             The contents can be retrieved or matched later in the string.
    (?aiLmsux) The letters set the corresponding flags defined below.
    (?:...)  Non-grouping version of regular parentheses.
    (?P<name>...) The substring matched by the group is accessible by name.
    (?P=name)     Matches the text matched earlier by the group named name.
    (?#...)  A comment; ignored.
    (?=...)  Matches if ... matches next, but doesn't consume the string.
    (?!...)  Matches if ... doesn't match next.
    (?<=...) Matches if preceded by ... (must be fixed length).
    (?<!...) Matches if not preceded by ... (must be fixed length).
    (?(id/name)yes|no) Matches yes pattern if the group with id/name matched,
                       the (optional) no pattern otherwise.

.. code-block:: sls

    match_plugin: regex
    kafka:
      "event_profile_.*":
        connection:
          bootstrap_servers: localhost:9092

The above profile will match and be used with the following event:

.. code-block:: python

    async def my_func(hub):
        await hub.evbus.broker.put(
            routing_key="foo", body="bar", profile="event_profile_baz"
        )
